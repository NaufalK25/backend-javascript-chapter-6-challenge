# Backend JavaScript Chapter 6 Challenge

Challenge for Chapter 6 from Binar Academy - Backend JavaScript class

### Prerequisites

1. [Git](https://git-scm.com/downloads)
    ```
    git --version
    ```
2. [Node.js](https://nodejs.org/en/)
    ```
    node -v
    ```
3. [PostgreSQL](https://www.postgresql.org/download/)
    ```
    psql --version
    ```
4. [Heroku](https://devcenter.heroku.com/articles/heroku-cli)
    ```
    heroku version
    ```

### How To Run In Local

1. Clone the repository
    ```
    git clone https://gitlab.com/NaufalK25/backend-javascript-chapter-6-challenge.git
    ```
2. Install dependencies
    ```
    npm install
    yarn
    ```
3. Create a database
    ```
    npm run db-init
    yarn run db-init
    ```
4. Run the server
    ```
    npm run dev
    yarn run dev
    ```

Status: `Development`

### Endpoint

### VIEW ENGINE

**GET** `/view`

**GET** `/view/user_games`

**GET** `/view/user_game/:id`

### Documentation

**GET** `/api-docs`

### API

**GET** `/api/v1`

#### User Gane

**GET** `/api/v1/user_games`

**POST** `/api/v1/user_games`

**GET** `/api/v1/user_game/:id`

**PATCH** `/api/v1/user_game/:id`

**DELETE** `/api/v1/user_game/:id`

#### User Game Biodata

**GET** `/api/v1/user_games/biodatas`

**POST** `/api/v1/user_games/biodatas`

**GET** `/api/v1/user_game/biodata/:id`

**PATCH** `/api/v1/user_game/biodata/:id`

**DELETE** `/api/v1/user_game/biodata/:id`

**GET** `/api/v1/user_game/:userGameId/biodata`

#### User Game History

**GET** `/api/v1/user_games/histories`

**POST** `/api/v1/user_games/histories`

**GET** `/api/v1/user_game/history/:id`

**PATCH** `/api/v1/user_game/history/:id`

**DELETE** `/api/v1/user_game/history/:id`

**GET** `/api/v1/user_game/:userGameId/history`
